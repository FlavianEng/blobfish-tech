import { Router } from 'express';
import pingRouter from './app/routes/ping.routes';
import orderRouter from './app/routes/order.routes';
import shippingRouter from './app/routes/shipping.routes';

const router = Router();

router.use('/ping', pingRouter);
router.use('/order', orderRouter);
router.use('/shipping', shippingRouter);

export default router;
