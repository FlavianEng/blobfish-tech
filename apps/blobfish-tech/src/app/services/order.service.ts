import {
  PurchasedProductDto,
  OrderCreatedDto,
  OrderDetailsDto,
} from '../dto/order.dto';
import * as orderRepository from '../repositories/order.repository';

// Create
export const create = async (
  purchasedProduct: PurchasedProductDto
): Promise<OrderCreatedDto> => {
  const orderCreated: OrderCreatedDto = await orderRepository.create(
    purchasedProduct
  );

  return orderCreated;
};

// Find one
export const findOne = async (id: string): Promise<OrderDetailsDto> => {
  const orderDetails: OrderDetailsDto = await orderRepository.findOne(id);

  return orderDetails;
};

// Find one
export const outForDelivery = async (id: string): Promise<OrderDetailsDto> => {
  const orderDetails: OrderDetailsDto = await orderRepository.outForDelivery(
    id
  );

  return orderDetails;
};

export const deleteAll = async (): Promise<void> => {
  const { deletedCount } = await orderRepository.deleteAll();
  console.info('🧨', deletedCount, 'item deleted');
};
