import { ShippingRequestDto } from '../dto/shipping.dto';
import * as shippingRepository from '../repositories/shipping.repository';

export const create = async (
  shippingRequest: ShippingRequestDto
): Promise<void> => {
  const shippingCreated = await shippingRepository.create(shippingRequest);

  return shippingCreated;
};
