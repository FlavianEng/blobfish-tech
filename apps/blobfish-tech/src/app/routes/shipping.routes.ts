import { Router } from 'express';
import * as shippingController from '../controllers/order.controller';

const router = Router();

router.post('/', shippingController.create);

export default router;
