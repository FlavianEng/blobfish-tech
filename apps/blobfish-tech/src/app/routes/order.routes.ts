import { Router } from 'express';
import * as orderController from '../controllers/order.controller';

const router = Router();

router.post('/', orderController.create);
router.get('/:id', orderController.getOnebyId);
router.put('/delivered/:id', orderController.outForDelivery);
router.delete('/deleteall', orderController.deleteAll);

export default router;
