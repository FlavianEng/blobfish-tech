import * as shippingService from '../services/shipping.service';
import { ShippingRequestDto } from '../dto/shipping.dto';

export const create = async (req, res) => {
  const shippingRequest: ShippingRequestDto = req.body;

  try {
    const shippingRequestCreated = await shippingService.create(
      shippingRequest
    );
    res.status(200).send(shippingRequestCreated);
  } catch (error) {
    res.status(400).send(error);
  }
};
