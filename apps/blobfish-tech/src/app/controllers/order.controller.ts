import { PurchasedProductDto } from '../dto/order.dto';
import * as orderService from '../services/order.service';

// Create
export const create = async (req, res) => {
  const purchasedProduct: PurchasedProductDto = req.body;

  try {
    const orderCreated = await orderService.create(purchasedProduct);
    res.status(201).send(orderCreated);
  } catch (error) {
    res.status(400).send(error);
  }
};

// Get one by id
export const getOnebyId = async (req, res) => {
  const { id } = req.params;

  try {
    const orderDetails = await orderService.findOne(id);
    res.status(200).send(orderDetails);
  } catch (error) {
    res.status(400).send(error);
  }
};

// Update order status
export const outForDelivery = async (req, res) => {
  const { id } = req.params;

  try {
    const orderDetails = await orderService.outForDelivery(id);
    res.status(200).send(orderDetails);
  } catch (error) {
    res.status(400).send(error);
  }
};

// Delete all
export const deleteAll = async (req, res) => {
  try {
    await orderService.deleteAll();
    res.status(200);
  } catch (err) {
    res.status(400);
    throw new Error(err.message);
  }
};
