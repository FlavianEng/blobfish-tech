import {
  PurchasedProductDto,
  OrderCreatedDto,
  OrderDetailsDto,
} from '../dto/order.dto';
import OrderModel from '../database/models/order.model';
import { DeleteResult } from 'mongodb';
import axios from 'axios';

// Save in the database
export const create = async (
  purchasedProduct: PurchasedProductDto
): Promise<OrderCreatedDto> => {
  try {
    // Reserve the product in the stock
    axios.post(
      `${process.env.API_STOCK}/${purchasedProduct.productId}/movement`,
      {
        productId: purchasedProduct.productId,
        quantity: purchasedProduct.quantity,
        status: 'Reserve',
      }
    );

    // Check if the product exist
    axios.get(`${process.env.API_PRODUCTS}/${purchasedProduct.productId}`);

    // Create order
    const orderCart = { products: [purchasedProduct] };
    const orderCreated: OrderCreatedDto = await new OrderModel(
      orderCart
    ).save();

    return orderCreated;
  } catch (error) {
    throw new Error(error.message);
  }
};

// Get one from the database
export const findOne = async (id: string): Promise<OrderDetailsDto> => {
  const orderDetails: OrderDetailsDto = await OrderModel.findById(id);

  return orderDetails;
};

// Update the status on delivered
export const outForDelivery = async (id: string): Promise<OrderDetailsDto> => {
  const update = { status: 'delivered' };
  const options = { new: true };
  const orderDetail: OrderDetailsDto = await OrderModel.findByIdAndUpdate(
    id,
    update,
    options
  );

  return orderDetail;
};

export const deleteAll = async (): Promise<DeleteResult> => {
  return await OrderModel.deleteMany({});
};
