import { ShippingRequestDto } from '../dto/shipping.dto';

import axios from 'axios';

export const create = async (
  shippingRequest: ShippingRequestDto
): Promise<void> => {
  try {
    const res = await axios.post(
      `^${process.env.API_SHIPPING}/shipping`,
      shippingRequest
    );
    console.log('🚀   res', res);
  } catch (error) {
    throw new Error(error.message);
  }
};
