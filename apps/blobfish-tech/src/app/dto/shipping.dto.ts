import { PurchasedProductDto } from './order.dto';

export interface ShippingRequestDto {
  orderId: string;
  nbProducts: number;
  products: PurchasedProductDto[];
}

export interface ShippingRequestCreatedDto {
  id: string;
}
