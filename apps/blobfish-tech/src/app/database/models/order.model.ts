import * as mongoose from 'mongoose';
import orderSchema from '../schemas/order.schema';

const OrderModel = mongoose.model('order', orderSchema);

export default OrderModel;
