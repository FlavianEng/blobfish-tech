import { Schema } from 'mongoose';

const orderSchema = new Schema({
  products: {
    type: Array,
    required: true,
  },
  status: {
    type: String,
    enum: { values: ['pending', 'delivered'] },
    default: 'pending',
  },
});

export default orderSchema;
