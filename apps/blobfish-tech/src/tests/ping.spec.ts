import * as request from 'supertest';
import { app } from '../app';

describe('/ping endpoint', () => {
  describe('[GET] Get the pong', () => {
    it('should have a 200', async () => {
      const {
        body: { message },
      } = await request(app).get(`/api/ping`).expect(200);

      expect(message).toEqual('Pong 🐡');
    });
  });
});
