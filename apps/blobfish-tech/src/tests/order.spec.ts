import { app } from '../app';
import * as request from 'supertest';
import * as dotenv from 'dotenv';

// Get the environment variable
dotenv.config({ path: process.cwd() + '/.env' });

// Example data
const examplePurchasedProduct = {
  productId: 'bobleponge',
  quantity: 2,
};

// Test
describe('/order endpoint', () => {
  beforeAll(async () => {
    await request(app).delete('/order/deleteall');
  });

  describe('[POST] When creating an order', () => {
    it('should pass', () => {
      const t = true;

      expect(t).toBe(true);
    });

    it('[POST] Should create an order', async () => {
      const { body } = await request(app)
        .post('/order')
        .send(examplePurchasedProduct)
        .expect(201);

      console.log('🚀   body', body);

      expect(body.id).toBeDefined();
    });
  });
});
