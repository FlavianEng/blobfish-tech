import * as mongoose from 'mongoose';
import * as dotenv from 'dotenv';

dotenv.config({ path: process.cwd() + '/.env' });

describe('Database', () => {
  it('Should connect to database', async () => {
    const { connections } = await mongoose.connect(
      `${process.env.MONGO_URL_TEST}`
    );

    expect(connections[0].readyState).toEqual(1);
  });
});
